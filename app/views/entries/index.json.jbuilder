json.array!(@entries) do |entry|
  json.extract! entry, :id, :name, :score
  json.url entry_url(entry, format: :json)
end
