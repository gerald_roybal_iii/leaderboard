class EntriesController < ApplicationController
  before_action :set_entry, only: [:show, :edit, :update, :destroy]

  # GET /entries
  # GET /entries.json
  def index
    @entries = Entry.all
  end

  # GET /entries/1
  # GET /entries/1.json
  def show
  end
  
  def get_rank
    @entries = Entry.all
    @entries.order!("score desc")
    
  end

  # GET /entries/new
  def new
   @entry = Entry.new
  end

  # GET /entries/1/edit
  def edit
  end

  # POST /entries
  # POST /entries.json
  def create
    if not (Entry.find_by(name: params[:entry][:name]))

      @entry = Entry.new(entry_params)
      
      @entries = Entry.all
      @entries.order!("score desc")
      
      respond_to do |format|
        if @entry.save
          
          format.html { redirect_to @entry, notice: 'Entry was successfully created.' }
          format.json { render :show, status: :created, location: @entry }
        else
          format.html { render :new }
          format.json { render json: @entry.errors, status: :unprocessable_entity }
        end
      end
    else
      Entry.find_by(name: params[:entry][:name]).update_attributes(:score => params[:entry][:score])
      
      @entries = Entry.all
      @entries.order!("score desc")
      
      redirect_to Entry.find_by(name: params[:entry][:name]), notice: 'User score updated!'
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    respond_to do |format|
      if @entry.update(entry_params)
        format.html { redirect_to @entry, notice: 'Entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry }
      else
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    respond_to do |format|
      format.html { redirect_to entries_url, notice: 'Entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(:name, :score)
    end
    
    def index_params
      params.permit(:offset, :number_of_entries)
    end
end
