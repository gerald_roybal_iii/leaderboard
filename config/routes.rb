Rails.application.routes.draw do
  resources :entries
  
  root 'entries#index' #root

  get 'index', to: 'entries#index' #no params, default values
  get 'index/:offset', to: 'entries#index' #offset specified
  get 'index/:offset/:number_of_entries', to: 'entries#index' #offset and number of entries specified
 
end
